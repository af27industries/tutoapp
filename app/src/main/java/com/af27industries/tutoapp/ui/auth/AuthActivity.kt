package com.af27industries.tutoapp.ui.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.af27industries.tutoapp.R

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }
}
